package core.api.dictionary.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Lukasz on 2018-04-22.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DictionaryGroupRequest {

    private String shortName;

    private String longName;

    private String description;

    private Long versionId;

}
