package core.api.dictionary.controller.impl;


import core.api.dictionary.controller.DictionaryController;
import core.api.dictionary.request.DictionaryRequest;
import core.domain.dictionary.Dictionary;
import core.service.dictionary.impl.DictionaryServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Lukasz on 2018-04-15.
 */
@Api(tags = "Dictionary endpoints")
@RestController
@RequestMapping("/dictionaries")
public class DictionaryControllerImpl implements DictionaryController {

    private final static Logger logger = LoggerFactory.getLogger(DictionaryControllerImpl.class);

    @Autowired
    private DictionaryServiceImpl dictionaryService;

    @ApiOperation(value = "Get all dictionaries")
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Dictionary> getDictionaries() {
        return dictionaryService.getDictionaries();
    }

    @ApiOperation("Get specific dictionary by ID param")
    @RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = "application/json")
    @ResponseBody
    public Dictionary getDictionaryById(@PathVariable("id") Long id) {
        return dictionaryService.getDictionaryById(id);
    }

    @ApiOperation("Create dictionary")
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Long createDictionary(@RequestBody DictionaryRequest dictionaryRequest) {
        logger.info("Create dictionary from dictionary request {}", dictionaryRequest);
        return dictionaryService.createDictionary(dictionaryRequest);
    }

}
