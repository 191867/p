package core.api.dictionary.controller;

import core.api.dictionary.request.DictionaryRequest;
import core.domain.dictionary.Dictionary;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Lukasz on 2018-04-24.
 */
public interface DictionaryController {

    List<Dictionary> getDictionaries();

    Dictionary getDictionaryById(Long id);

    Long createDictionary(@RequestBody DictionaryRequest dictionaryRequest);

}
