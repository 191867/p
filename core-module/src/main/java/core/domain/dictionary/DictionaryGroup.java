package core.domain.dictionary;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.neo4j.ogm.annotation.*;
import org.neo4j.ogm.id.InternalIdStrategy;
import org.springframework.data.annotation.Version;

/**
 * Created by Lukasz on 2018-04-17.
 */
@Getter
@Setter
@NoArgsConstructor
@NodeEntity(label = "DictionaryGroup")
public class DictionaryGroup {

    @Id
    @GeneratedValue(strategy = InternalIdStrategy.class)
    private Long id;

    @Property(name = "SHORT_NAME")
    private String shortName;

    @Property(name = "LONG_NAME")
    private String longName;

    @Property(name = "DESCRIPTION")
    private String description;

    @Version
    @Property(name = "VERSION_ID")
    private Long versionId;

}
