package core.service.dictionary.impl;

import core.api.dictionary.request.DictionaryRequest;
import core.domain.dictionary.Dictionary;
import core.mapper.dictionary.DictionaryMapper;
import core.service.dictionary.DictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import core.repository.dictionary.DictionaryRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Lukasz on 2018-04-15.
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class DictionaryServiceImpl implements DictionaryService {

    @Autowired
    private DictionaryRepository dictionaryRepository;

    @Autowired
    private DictionaryMapper dictionaryMapper;

    public List<Dictionary> getDictionaries() {
        return (List<Dictionary>) dictionaryRepository.findAll();
    }

    public Dictionary getDictionaryById(Long ppid) {
        return dictionaryRepository.findOne(ppid);
    }

    public Long createDictionary(DictionaryRequest dictionaryRequest) {
        Dictionary dictionary = dictionaryMapper.mapToEntity(dictionaryRequest);
        dictionaryRepository.save(dictionary);
        return dictionary.getId();
    }

    public Long fakeCreateDictionary(Dictionary dic) {
        Dictionary dictionary = dictionaryRepository.save(dic);
        return dictionary.getId();
    }
}