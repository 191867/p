package core.service.dictionary;

import core.api.dictionary.request.DictionaryRequest;
import core.domain.dictionary.Dictionary;

import java.util.List;

/**
 * Created by Lukasz on 2018-04-24.
 */
public interface DictionaryService {

    List<Dictionary> getDictionaries();

    Dictionary getDictionaryById(Long ppid);

    Long createDictionary(DictionaryRequest dictionaryRequest);

    Long fakeCreateDictionary(Dictionary dic);

}
