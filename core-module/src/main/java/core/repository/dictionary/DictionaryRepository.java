package core.repository.dictionary;

import core.domain.dictionary.Dictionary;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Created by Lukasz on 2018-04-15.
 */
public interface DictionaryRepository extends Neo4jRepository<Dictionary, Long> {

}
