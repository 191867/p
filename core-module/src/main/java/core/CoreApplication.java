package core;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;

/**
 * Created by Lukasz on 2018-04-15.
 */

@SpringBootApplication
@EnableNeo4jRepositories("core.repository")
public class CoreApplication {
}
