package core.dto.dictionary;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Lukasz on 2018-04-22.
 */
@Getter
@Setter
public class DictionaryData {

    private Long id;

    private String shortName;

    private String longName;

    private String description;

    private Long versionId;
}
