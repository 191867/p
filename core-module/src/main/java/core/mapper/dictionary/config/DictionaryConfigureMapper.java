package core.mapper.dictionary.config;

import core.api.dictionary.request.DictionaryRequest;
import core.domain.dictionary.Dictionary;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

/**
 * Created by Lukasz on 2018-04-24.
 */
public class DictionaryConfigureMapper {

    protected final ModelMapper mapper = new ModelMapper();

    protected void configureMapper() {
        PropertyMap<DictionaryRequest, Dictionary> propertyMap = new PropertyMap<DictionaryRequest, Dictionary>() {
            @Override
            protected void configure() {
                skip(null, destination.getId());
            }
        };
        mapper.addMappings(propertyMap);
    }

}
