package core.mapper.dictionary;

import core.api.dictionary.request.DictionaryGroupRequest;
import core.api.dictionary.request.DictionaryRequest;
import core.domain.dictionary.Dictionary;
import core.domain.dictionary.DictionaryGroup;
import core.dto.dictionary.DictionaryData;
import core.dto.dictionary.DictionaryGroupData;

/**
 * Created by Lukasz on 2018-04-22.
 */
public interface DictionaryMapper {

    Dictionary mapToEntity(DictionaryRequest dictionaryRequest);

    DictionaryData mapToDto(Dictionary dictionary);

    DictionaryGroup mapToEntity(DictionaryGroupRequest dictionaryGroupRequest);

    DictionaryGroupData mapToDto(DictionaryGroup dictionaryGroup);

}
