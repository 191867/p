package core.mapper.dictionary.impl;

import core.api.dictionary.request.DictionaryGroupRequest;
import core.api.dictionary.request.DictionaryRequest;
import core.domain.dictionary.Dictionary;
import core.domain.dictionary.DictionaryGroup;
import core.dto.dictionary.DictionaryData;
import core.dto.dictionary.DictionaryGroupData;
import core.mapper.dictionary.DictionaryMapper;
import core.mapper.dictionary.config.DictionaryConfigureMapper;
import org.springframework.stereotype.Component;

/**
 * Created by Lukasz on 2018-04-22.
 */
@Component
public class DictionaryMapperImpl extends DictionaryConfigureMapper implements DictionaryMapper {

    public DictionaryMapperImpl() {
        configureMapper();
    }

    @Override
    public Dictionary mapToEntity(DictionaryRequest dictionaryRequest) {
        return mapper.map(dictionaryRequest, Dictionary.class);
    }

    @Override
    public DictionaryData mapToDto(Dictionary dictionary) {
        return mapper.map(dictionary, DictionaryData.class);
    }

    @Override
    public DictionaryGroup mapToEntity(DictionaryGroupRequest dictionaryGroupRequest) {
        return mapper.map(dictionaryGroupRequest, DictionaryGroup.class);
    }

    @Override
    public DictionaryGroupData mapToDto(DictionaryGroup dictionaryGroup) {
        return mapper.map(dictionaryGroup, DictionaryGroupData.class);
    }
}
